import {
  Box,
  Button,
  Flex,
  HStack,
  IconButton,
  Text,
  useColorModeValue as mode,
} from '@chakra-ui/react';
import { useWeb3React } from '@web3-react/core';
import { useRouter } from 'next/router';
import { FaDiscord, FaTwitter } from 'react-icons/fa';

import { useWalletDialog } from '../../context';
import { getIcon } from '../../theme';
import AccountPopover from '../Popover/AccountPopover';
import { MobileNav } from './MobileNav';
import { NavLink } from './NavLink';

export default function Navbar() {
  const { account } = useWeb3React();
  const [, setWalletDialogIsOpen] = useWalletDialog();
  const router = useRouter();

  return (
    <Box
      as="header"
      bg={mode('white', 'gray.800')}
      borderBottomWidth="1px"
      h="4.5rem"
    >
      <Box maxW="7xl" mx="auto" py="4" px={{ base: '6', md: '8' }}>
        <Flex as="nav" justify="space-between">
          <HStack spacing="8">
            <Text
              fontSize="1.5rem"
              fontWeight="500"
              onClick={() => router.push('/')}
              cursor="pointer"
            >
              Hashes
            </Text>
            <HStack display={{ base: 'none', md: 'flex' }} spacing="8">
              <NavLink.Desktop
                active={router.pathname === '/'}
                onClick={() => router.push('/')}
              >
                Mint
              </NavLink.Desktop>

              <NavLink.Desktop
                active={router.pathname === '/about'}
                onClick={() => router.push('/about')}
              >
                About
              </NavLink.Desktop>
              <NavLink.Desktop
                active={router.pathname === '/perceive'}
                onClick={() => router.push('/perceive')}
              >
                Perceive
              </NavLink.Desktop>
            </HStack>
          </HStack>
          <Flex align="center">
            <HStack spacing="4" display={{ base: 'none', md: 'flex' }}>
              <IconButton
                color={mode('gray.600', 'gray.300')}
                as="a"
                href="https://twitter.com/HashesDAO"
                aria-label="Twitter"
                icon={<FaTwitter size={18} />}
                target="_blank"
                rel="noreferrer noopener"
                isRound
                size="sm"
              />
              <IconButton
                color={mode('gray.600', 'gray.300')}
                as="a"
                href="https://discord.gg/Qxb67nf3U2"
                aria-label="Discord"
                icon={<FaDiscord size={20} />}
                target="_blank"
                rel="noreferrer noopener"
                isRound
                size="sm"
              />
              <IconButton
                color={mode('gray.600', 'gray.300')}
                as="a"
                href="https://opensea.io/collection/hashes"
                aria-label="OpenSea"
                icon={getIcon('OpenSea', 5)}
                target="_blank"
                rel="noreferrer noopener"
                isRound
                size="sm"
              />
              <IconButton
                color={mode('gray.600', 'gray.300')}
                as="a"
                href="https://etherscan.io/address/0xD07e72b00431af84AD438CA995Fd9a7F0207542d"
                aria-label="Etherscan"
                icon={getIcon('Etherscan', 5)}
                target="_blank"
                rel="noreferrer noopener"
                isRound
                size="sm"
              />

              {account ? (
                <AccountPopover />
              ) : (
                <Button onClick={() => setWalletDialogIsOpen(true)}>
                  Connect wallet
                </Button>
              )}
            </HStack>
            <Box ml="5">
              <MobileNav />
            </Box>
          </Flex>
        </Flex>
      </Box>
    </Box>
  );
}
