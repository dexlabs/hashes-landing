import {
  Box,
  Button,
  Flex,
  HStack,
  IconButton,
  Link,
  Portal,
  SimpleGrid,
  Text,
  useBoolean,
  useColorModeValue as mode,
  useFocusOnShow,
  VStack,
} from '@chakra-ui/react';
import { useWeb3React } from '@web3-react/core';
import { HTMLMotionProps, motion, Variants } from 'framer-motion';
import { useRouter } from 'next/router';
import { useRef } from 'react';
import FocusLock from 'react-focus-lock';
import {
  AiOutlineBorderlessTable,
  AiOutlineClose,
  AiOutlineEye,
  AiOutlineMenu,
  AiOutlineQuestionCircle,
} from 'react-icons/ai';
import { RemoveScroll } from 'react-remove-scroll';

import { useWalletDialog } from '../../context';
import { NavLink } from './NavLink';

const variants: Variants = {
  show: {
    display: 'revert',
    opacity: 1,
    scale: 1,
    transition: { duration: 0.2, ease: 'easeOut' },
  },
  hide: {
    opacity: 0,
    scale: 0.98,
    transition: { duration: 0.1, ease: 'easeIn' },
    transitionEnd: { display: 'none' },
  },
};

const Backdrop = ({ show }: { show?: boolean }) => (
  <Portal>
    <motion.div
      initial={false}
      animate={show ? 'show' : 'hide'}
      transition={{ duration: 0.1 }}
      variants={{
        show: { opacity: 1, display: 'revert' },
        hide: { opacity: 0, transitionEnd: { display: 'none' } },
      }}
      style={{
        width: '100%',
        height: '100vh',
        position: 'absolute',
        background: 'rgba(0,0,0,0.2)',
        inset: 0,
      }}
    />
  </Portal>
);

const Transition = (props: HTMLMotionProps<'div'> & { in?: boolean }) => {
  const { in: inProp, ...rest } = props;
  return (
    <motion.div
      {...rest}
      initial={false}
      variants={variants}
      animate={inProp ? 'show' : 'hide'}
      style={{
        transformOrigin: 'top right',
        position: 'absolute',
        width: 'calc(100% - 32px)',
        top: '24px',
        left: '16px',
        margin: '0 auto',
        zIndex: 1,
      }}
    />
  );
};

export const MobileNav = () => {
  const [show, { toggle, off }] = useBoolean();
  const ref = useRef<HTMLDivElement>(null);
  useFocusOnShow(ref, { visible: show, shouldFocus: true });

  const route = useRouter();
  const [, setWalletDialogIsOpen] = useWalletDialog();
  const router = useRouter();

  function handleOnClick(route: string) {
    router.push(route);
    off();
  }

  return (
    <>
      <IconButton
        size="sm"
        color="gray.400"
        onClick={toggle}
        display={{ base: 'flex', md: 'none' }}
        aria-label="open menu"
        icon={<AiOutlineMenu />}
      />

      <Transition in={show}>
        <RemoveScroll enabled={show}>
          <Backdrop show={show} />
        </RemoveScroll>
        <FocusLock disabled={!show} returnFocus>
          <Box
            bg={mode('white', 'gray.700')}
            shadow="lg"
            rounded="lg"
            ref={ref}
            tabIndex={0}
            outline={0}
          >
            <Box pt="5" pb="6" px="5">
              <Flex justify="space-between" align="center">
                <Text
                  fontSize="1.5rem"
                  fontWeight="500"
                  onClick={() => route.push('/')}
                  cursor="pointer"
                >
                  Hashes
                </Text>
                <Box mr="-2" mt="-2">
                  <IconButton
                    size="sm"
                    color="gray.400"
                    onClick={off}
                    display={{ base: 'flex', md: 'none' }}
                    aria-label="close menu"
                    icon={<AiOutlineClose />}
                  />
                </Box>
              </Flex>
              <SimpleGrid as="nav" gap="6" mt="8" columns={{ base: 1, sm: 2 }}>
                <NavLink.Mobile
                  icon={AiOutlineBorderlessTable}
                  active={router.pathname === '/'}
                  onClick={() => handleOnClick('/')}
                >
                  Mint
                </NavLink.Mobile>

                <NavLink.Mobile
                  icon={AiOutlineQuestionCircle}
                  active={router.pathname === '/about'}
                  onClick={() => handleOnClick('/about')}
                >
                  About
                </NavLink.Mobile>
                <NavLink.Mobile
                  icon={AiOutlineEye}
                  active={router.pathname === '/perceive'}
                  onClick={() => handleOnClick('/perceive')}
                >
                  Perceive
                </NavLink.Mobile>
              </SimpleGrid>

              <VStack mt="8" spacing="4">
                <Button
                  w="full"
                  colorScheme="gray"
                  onClick={() => setWalletDialogIsOpen(true)}
                >
                  Connect wallet
                </Button>
                <Flex
                  fontWeight="medium"
                  w="100%"
                  justifyContent="space-between"
                >
                  <Link
                    href="https://opensea.io/collection/hashes"
                    isExternal
                    color={mode('gray.600', 'gray.400')}
                  >
                    OpenSea
                  </Link>
                  {/* <Link
                    href="https://etherscan.io/address/0xD07e72b00431af84AD438CA995Fd9a7F0207542d"
                    isExternal
                    color={mode('gray.600', 'gray.400')}
                  >
                    Etherscan
                  </Link> */}

                  <Link
                    href="https://twitter.com/HashesDAO"
                    isExternal
                    color={mode('gray.600', 'gray.400')}
                  >
                    Twitter
                  </Link>
                  <Link
                    href="https://discord.gg/Qxb67nf3U2"
                    isExternal
                    color={mode('gray.600', 'gray.400')}
                  >
                    Discord
                  </Link>
                </Flex>
              </VStack>
            </Box>
          </Box>
        </FocusLock>
      </Transition>
    </>
  );
};
