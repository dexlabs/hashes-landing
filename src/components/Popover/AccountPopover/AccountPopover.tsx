import {
  Box,
  Button,
  ButtonGroup,
  Flex,
  IconButton,
  Popover,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverFooter,
  PopoverHeader,
  PopoverTrigger,
  Text,
  useClipboard,
  useColorModeValue as mode,
} from '@chakra-ui/react';
import { useWeb3React } from '@web3-react/core';
import { constants } from 'ethers';
import { useEffect, useState } from 'react';
import { FiClipboard, FiPaperclip } from 'react-icons/fi';

import { injected, walletconnect } from '../../../config/connectors';
import { useWalletDialog } from '../../../context';
import { getIcon } from '../../../theme';
import { getEtherscanLink, shortenAddress } from '../../../utils';
import formatConnectorName from './formatConnectorName';

export default function AccountPopover() {
  const { account, library, chainId, connector } = useWeb3React();
  const accountClipboard = useClipboard(account || constants.AddressZero);
  const [ENSName, setENSName] = useState('');
  const [, setWalletDialogIsOpen] = useWalletDialog();

  useEffect(() => {
    if (library && account) {
      let stale = false;
      library
        .lookupAddress(account)
        .then((name: string | null) => {
          if (!stale && typeof name === 'string') {
            if (name.length > 12) setENSName(name.substr(0, 8) + '...');
            else setENSName(name);
          }
        })
        .catch(() => {});
      return (): void => {
        stale = true;
        setENSName('');
      };
    }
  }, [library, account, chainId]);

  return (
    <Popover>
      <PopoverTrigger>
        <IconButton
          color={mode('gray.600', 'gray.300')}
          isRound
          icon={getIcon('Avatar')}
          aria-label="Account"
          size="sm"
          height="2rem"
          width="2rem"
          ml="0.25rem"
        />
      </PopoverTrigger>
      <PopoverContent bgColor="background.100">
        <PopoverHeader fontWeight="semibold">Account</PopoverHeader>
        <PopoverCloseButton />
        <PopoverBody>
          {!!account && (
            <Flex direction="column" fontSize="1.25rem">
              <Flex
                align="center"
                flexWrap="nowrap"
                justifyContent="space-between"
              >
                <Box fontSize="0.75rem">{formatConnectorName(connector)}</Box>
              </Flex>

              <Text>{ENSName || `${shortenAddress(account)}`}</Text>

              <ButtonGroup size="xs" spacing="2">
                <IconButton
                  fontWeight="300"
                  aria-label="copy address"
                  icon={
                    accountClipboard.hasCopied ? (
                      <FiPaperclip />
                    ) : (
                      <FiClipboard />
                    )
                  }
                  onClick={accountClipboard.onCopy}
                />
                <Button
                  as="a"
                  target="_blank"
                  href={
                    chainId
                      ? getEtherscanLink(chainId, account, 'address')
                      : '#'
                  }
                  rel="noopener noreferrer"
                  aria-label="View on Etherscan"
                  leftIcon={getIcon('Etherscan', 4)}
                  fontWeight="300"
                >
                  View on Etherscan
                </Button>
              </ButtonGroup>
            </Flex>
          )}
        </PopoverBody>
        <PopoverFooter d="flex" justifyContent="space-between">
          <Button size="sm" onClick={() => setWalletDialogIsOpen(true)}>
            Change wallets
          </Button>
          <Button
            size="sm"
            onClick={() => (connector as any).close()}
            display={connector === walletconnect ? 'block' : 'none'}
          >
            Disconnect
          </Button>
        </PopoverFooter>
      </PopoverContent>
    </Popover>
  );
}
