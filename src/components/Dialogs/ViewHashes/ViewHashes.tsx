import { Box, ListItem, Text, UnorderedList } from '@chakra-ui/react';
import { useWeb3React } from '@web3-react/core';
import { useEffect, useState } from 'react';

import { useViewHashesDialog } from '../../../context';
import HASHES_ABI from '../../../Hashes.json';
import { useContract } from '../../../hooks';
import { HASHES_ADDRESS, shortenAddress } from '../../../utils';
import Modal from '../Dialog';

export default function ViewHashes() {
  const { account, chainId } = useWeb3React();
  const [viewHashesDialogIsOpen, setViewHashesDialogIsOpen] =
    useViewHashesDialog();

  const HashesContract = useContract(
    chainId && HASHES_ADDRESS[chainId],
    HASHES_ABI.abi,
    true,
  );
  const [hashes, setHashes] = useState<string[]>([]);

  useEffect(() => {
    (async () => {
      if (HashesContract && account) {
        try {
          const balance = await HashesContract.balanceOf(account);

          const hashes: string[] = [];
          for (let i = 0; i < balance.toNumber(); i++) {
            const tokenId = await HashesContract.tokenOfOwnerByIndex(
              account,
              i,
            );
            const hash = await HashesContract.getHash(tokenId);
            hashes.push(hash);
          }

          setHashes(hashes);
        } catch (error: any) {
          console.error(error);
        }
      }
    })();
  }, [account, HashesContract]);

  return (
    <Modal
      isOpen={viewHashesDialogIsOpen}
      onClose={() => setViewHashesDialogIsOpen(false)}
      header="Your Hashes"
      mb="1rem"
    >
      <Box>
        {hashes.length === 0 ? (
          <Text>No Hashes to display</Text>
        ) : (
          <UnorderedList>
            {hashes.map((hash: string, i: number) => {
              // console.log('hash:', hash);
              return <ListItem key={i}>{shortenAddress(hash)}</ListItem>;
            })}
          </UnorderedList>
        )}
      </Box>
    </Modal>
  );
}
