export const shortenAddress = (address: string, length = 4): string => {
  return `${address.slice(0, length + 2)}…${address.slice(
    address.length - length,
  )}`;
};

enum ChainId {
  MAINNET = 1,
  ROPSTEN = 3,
  RINKEBY = 4,
  GORLI = 5,
  KOVAN = 42,
}

export const CHAIN_ID_NAMES: { [key: number]: string } = {
  1: 'Mainnet',
  3: 'Ropsten',
  4: 'Rinkeby',
  5: 'Gorli',
  42: 'Kovan',
};

const ETHERSCAN_PREFIXES: { [chainId in ChainId]: string } = {
  1: '',
  3: 'ropsten.',
  4: 'rinkeby.',
  5: 'gorli.',
  42: 'kovan.',
};

export function getEtherscanLink(
  chainId: ChainId,
  data: string,
  type: 'transaction' | 'token' | 'address' | 'block',
): string {
  const prefix = `https://${
    ETHERSCAN_PREFIXES[chainId] || ETHERSCAN_PREFIXES[1]
  }etherscan.io`;

  switch (type) {
    case 'transaction': {
      return `${prefix}/tx/${data}`;
    }
    case 'token': {
      return `${prefix}/token/${data}`;
    }
    case 'block': {
      return `${prefix}/block/${data}`;
    }
    case 'address':
    default: {
      return `${prefix}/address/${data}`;
    }
  }
}

export const HASHES_ADDRESS = {
  [ChainId.MAINNET]: '0xD07e72b00431af84AD438CA995Fd9a7F0207542d',
  [ChainId.RINKEBY]: '0x1dBBf7dDf9f301523761Bccea9Dc1010F1E77833',
  [ChainId.KOVAN]: '0x4c4001853e0AF12Baf0578e1ECeA8ED030EC3FB3',
};

export function hex2bin(hex) {
  return hex
    .replace('0x', '')
    .split('')
    .map((i) => parseInt(i, 16).toString(2).padStart(4, '0'))
    .join('');
}
