import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useContext,
  useMemo,
  useState,
} from 'react';

const LayoutContext = createContext<
  [
    {
      walletDialogIsOpen: any;
      viewHashesDialogIsOpen: any;
      selectedArt: any;
      selectedHash: any;
    },
    {
      setWalletDialogIsOpen: Dispatch<SetStateAction<any>>;
      setViewHashesDialogIsOpen: Dispatch<SetStateAction<any>>;
      setSelectedArt: Dispatch<SetStateAction<any>>;
      setSelectedHash: Dispatch<SetStateAction<any>>;
    },
  ]
>([{}, {}] as any);

function useLayoutContext() {
  return useContext(LayoutContext);
}

export default function LayoutProvider({
  children,
}: {
  children: ReactNode;
}): JSX.Element {
  const [selectedArt, setSelectedArt] = useState('');
  const [selectedHash, setSelectedHash] = useState('');
  const [walletDialogIsOpen, setWalletDialogIsOpen] = useState(false);
  const [viewHashesDialogIsOpen, setViewHashesDialogIsOpen] = useState(false);

  return (
    <LayoutContext.Provider
      value={useMemo(
        () => [
          {
            walletDialogIsOpen,
            selectedArt,
            selectedHash,
            viewHashesDialogIsOpen,
          },
          {
            setWalletDialogIsOpen,
            setSelectedArt,
            setSelectedHash,
            setViewHashesDialogIsOpen,
          },
        ],
        [
          walletDialogIsOpen,
          setWalletDialogIsOpen,
          selectedArt,
          setSelectedArt,
          selectedHash,
          setSelectedHash,
          viewHashesDialogIsOpen,
          setViewHashesDialogIsOpen,
        ],
      )}
    >
      {children}
    </LayoutContext.Provider>
  );
}

export function useWalletDialog() {
  const [{ walletDialogIsOpen }, { setWalletDialogIsOpen }] =
    useLayoutContext();
  return [walletDialogIsOpen, setWalletDialogIsOpen];
}

export function useViewHashesDialog() {
  const [{ viewHashesDialogIsOpen }, { setViewHashesDialogIsOpen }] =
    useLayoutContext();
  return [viewHashesDialogIsOpen, setViewHashesDialogIsOpen];
}

export function useSelectedArt() {
  const [{ selectedArt }, { setSelectedArt }] = useLayoutContext();
  return [selectedArt, setSelectedArt];
}

export function useSelectedHash() {
  const [{ selectedHash }, { setSelectedHash }] = useLayoutContext();
  return [selectedHash, setSelectedHash];
}
