import { useWeb3React } from '@web3-react/core';
import { ethers } from 'ethers';
import { useEffect, useState } from 'react';

export default (eventFilter: ethers.EventFilter, fromBlock?: number) => {
  const { library } = useWeb3React<ethers.providers.Web3Provider>();
  const [eventLogs, setEventLogs] = useState<any[]>([]);
  const [error, setError] = useState<number | null>(null);

  useEffect(() => {
    if (library) {
      const filter = {
        ...eventFilter,
        fromBlock,
      };

      try {
        library.getLogs(filter).then((l) => setEventLogs(l));
        setError(null);
      } catch (e: any) {
        setError(e.code || -1);
      }

      const eventCb = (l: any) => {
        setEventLogs([...eventLogs, l]);
      };
      try {
        library.on(filter, eventCb);
        setError(null);
      } catch (e: any) {
        setError(e.code || -1);
      }

      return () => {
        library.removeListener(filter, eventCb);
      };
    }
  }, [eventFilter, fromBlock, eventLogs, library]);

  return { eventLogs, error };
};
