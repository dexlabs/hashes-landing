import { ChakraProvider } from '@chakra-ui/react';
import { Web3Provider } from '@ethersproject/providers';
import { Web3ReactProvider } from '@web3-react/core';
import Head from 'next/head';

import Layout from '../components/Layout';
import Web3Manager from '../components/Web3Manager';
import LayoutProvider from '../context';
import { theme } from '../theme';

function getLibrary(provider: any): Web3Provider {
  return new Web3Provider(provider);
}

export default function App({ Component, pageProps }: any) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
        <title>Hashes</title>
        <meta
          name="description"
          content="Hashes are foundational NFT constructs, a source of entropy and a versatile network of distribution for content creators."
        />
        <link rel="shortcut icon" href="/favicon.ico" />

        {/* Twitter */}
        <meta name="twitter:card" content="summary" key="twcard" />
        <meta name="twitter:creator" content="@HashesDAO" key="twhandle" />
        <meta name="twitter:site" content="@HashesDAO" />
        <meta name="twitter:title" content="Hashes" />
        <meta
          name="twitter:description"
          content="Hashes are foundational NFT constructs, a source of entropy and a versatile network of distribution for content creators."
        />
        <meta
          name="twitter:image"
          content="https://gitlab.com/brennanfife/hashes_twitter_card_image/-/raw/main/hashes_card.jpg"
        />

        {/* OpenGraph */}
        <meta
          property="og:url"
          content="https://www.thehashes.xyz/"
          key="ogurl"
        />
        <meta
          property="og:image"
          content="https://gitlab.com/brennanfife/hashes_twitter_card_image/-/raw/main/hashes_card.jpg"
          key="ogimage"
        />
        <meta property="og:site_name" content="HashesDAO" key="ogsitename" />
        <meta property="og:title" content="Hashes NFT" key="ogtitle" />
        <meta
          property="og:description"
          content="Hashes are foundational NFT constructs, a source of entropy and a versatile network of distribution for content creators."
          key="ogdesc"
        />
      </Head>
      <Web3ReactProvider getLibrary={getLibrary}>
        <ChakraProvider theme={theme}>
          <LayoutProvider>
            <Web3Manager>
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </Web3Manager>
          </LayoutProvider>
        </ChakraProvider>
      </Web3ReactProvider>
    </>
  );
}
