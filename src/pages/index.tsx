import {
  Box,
  Button,
  FormControl,
  Heading,
  Input,
  Stack,
  Text,
  Tooltip,
  useColorModeValue,
  useToast,
} from '@chakra-ui/react';
import { useWeb3React } from '@web3-react/core';
import { ethers } from 'ethers';
import { ChangeEvent, useEffect, useState } from 'react';

import { useViewHashesDialog } from '../context';
import HASHES_ABI from '../Hashes.json';
import { useContract } from '../hooks';
import { HASHES_ADDRESS } from '../utils';
import { MetaMaskErrors } from '../utils/constants';

export default function Mint() {
  const { active, chainId, account, library } = useWeb3React();

  const HashesContract = useContract(
    chainId && HASHES_ADDRESS[chainId],
    HASHES_ABI.abi,
    true,
  );
  const [, setViewHashesDialogIsOpen] = useViewHashesDialog();
  const [hash, setHash] = useState('');
  const toast = useToast();
  const [loading, setLoading] = useState(false);
  const [disableMintButton, setDisableMintButton] = useState(false);
  const [mintFee, setMintFee] = useState();

  useEffect(() => {
    const currentMintFee = mintFee;
    if (account && currentMintFee !== undefined) {
      (async () => {
        const balance = await library.getBalance(account);
        if (balance < currentMintFee) setDisableMintButton(true);
      })();
    }
  }, [account, library, mintFee]);

  useEffect(() => {
    (async () => {
      if (!HashesContract) {
        return;
      }
      const mintFee = await HashesContract.mintFee();
      setMintFee(mintFee);
    })();
  }, [HashesContract]);

  async function MintHash(mintFee) {
    if (HashesContract && mintFee !== undefined) {
      setLoading(true);
      try {
        const tx = await HashesContract.generate(hash, {
          value: mintFee.toString(),
        });
        await tx.wait();
        toast({
          title: 'Hash created.',
          description: `Successfully minted: ${hash}.`,
          status: 'success',
          isClosable: true,
          variant: 'left-accent',
          position: 'bottom-right',
        });
      } catch (error: any) {
        const errorMessage = error.message.includes(
          MetaMaskErrors.userRejectsTxSignature,
        )
          ? 'User rejected request'
          : error.message.includes(MetaMaskErrors.userRejectsMessageSignature)
          ? 'User denied message signature'
          : 'Please try again';
        toast({
          title: 'Minting failed.',
          description: `${errorMessage}.`,
          status: 'error',
          isClosable: true,
          variant: 'left-accent',
          position: 'bottom-right',
        });
      }
      setLoading(false);
    }
  }

  const currentMintFee = mintFee;
  return (
    <Box
      as="section"
      bgColor="gray.500"
      py="20"
      minH="calc(100vh - 4.5rem)"
      px={{ base: '2rem', md: '0rem' }}
    >
      <Box
        bg={useColorModeValue('white', 'gray.800')}
        rounded="lg"
        p="8"
        maxW="2xl"
        mx="auto"
        textAlign="center"
      >
        <Stack maxW="sm" mx="auto" spacing="8">
          <Stack spacing="2">
            <Text fontSize="1.5rem" fontWeight="500">
              Hashes
            </Text>
            <Heading as="h1" letterSpacing="tight">
              Mint a Hash
            </Heading>
            <Text
              fontWeight="medium"
              color={useColorModeValue('gray.600', 'gray.400')}
            >
              Use a phrase to generate a Hash. <br />
              Note, it will be visible to the public.
            </Text>
          </Stack>

          <FormControl>
            <Input
              placeholder="Your next hash..."
              value={hash}
              focusBorderColor="gray.500"
              onChange={(e: ChangeEvent<HTMLInputElement>) =>
                setHash(e.target.value)
              }
              isDisabled={!active}
              _placeholder={{
                color: useColorModeValue('gray.600', 'gray.400'),
              }}
              aria-label="Word to hash"
            />
          </FormControl>
          <Tooltip
            label={`Mint Fee: ${
              currentMintFee !== undefined
                ? `${ethers.utils.formatEther(currentMintFee)} ETH`
                : 'loading...'
            }`}
            shouldWrapChildren
          >
            <Button
              isFullWidth
              fontSize="sm"
              fontWeight="bold"
              colorScheme="gray"
              isDisabled={!active || !!!hash || disableMintButton}
              onClick={() => MintHash(mintFee)}
              isLoading={loading}
              loadingText="Generating Hash"
            >
              Generate Hash
            </Button>
          </Tooltip>

          <Box fontSize="sm">
            <Text
              fontWeight="medium"
              color={useColorModeValue('gray.600', 'gray.400')}
            >
              Already have Hashes?
            </Text>
            <Button
              fontWeight="semibold"
              color={useColorModeValue('gray.600', 'gray.300')}
              onClick={() => setViewHashesDialogIsOpen(true)}
              isDisabled={!active}
              variant="link"
              fontSize="0.8rem"
            >
              View them
            </Button>
          </Box>
        </Stack>
        {/* <Text
          mt="12"
          fontSize="xs"
          mx="auto"
          maxW="md"
          color={useColorModeValue('gray.600', 'gray.400')}
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora
          aspernatur nostrum fugit minima, voluptatum error eaque aperiam
          laudantium! Beatae animi quos sapiente facere esse.
        </Text> */}
      </Box>
    </Box>
  );
}
