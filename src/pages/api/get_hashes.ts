import { CosmosClient } from "@azure/cosmos";

export default async function handler(req, res) {
    const endpoint = process.env.DB_ENDPOINT;
    const key = process.env.DB_KEY;
    if (!endpoint || !key) {
        res.status(400).send();
        return;
    }

    const client = new CosmosClient({ endpoint, key });

    const querySpec = {
        query: "SELECT t.tokenId, t.hash FROM t"
    };

    const dbResult = await client
        .database("hashes")
        .container("scraped_data")
        .items
        .query(querySpec)
        .fetchAll();

    if (!dbResult || !Array.isArray(dbResult.resources)) {
        res.status(400).send();
        return;
    }

    res.setHeader('Cache-Control', 's-maxage=3600');
    res.status(200).json(dbResult.resources.map(hash => ({
        tokenId: hash.tokenId,
        hash: hash.hash
    })));
}