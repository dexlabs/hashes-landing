import { CosmosClient } from "@azure/cosmos";
import { ethers } from 'ethers';

import { INFURA_PREFIXES } from '../../../config/connectors';
import HASHES_ABI from '../../../Hashes.json';
import { HASHES_ADDRESS, hex2bin } from '../../../utils';
import { HASH_ATTRIBUTES } from '../../../utils/hash_attributes';

function getHashesContract(chain_id: string | undefined) {
    // Use chain id from by query param, but fallback to environment variable or mainnet
    const chainId = chain_id && INFURA_PREFIXES[chain_id] && HASHES_ADDRESS[chain_id]
        ? chain_id
        : process.env.API_CHAIN_ID || 1;
    const provider = new ethers.providers.InfuraProvider(INFURA_PREFIXES[chainId], process.env.API_INFURA_PROJECT_ID);
    return new ethers.Contract(HASHES_ADDRESS[chainId], HASHES_ABI.abi, provider);
}

function getBase64EncodedSVG(tokenId: number, hash: string): string {
    const svgHTML = `<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMinYMin meet" viewBox="0 0 350 350"><defs><filter id="f1" x="0" y="0" height="100%" width="100%"><feGaussianBlur in="SourceGraphic" stdDeviation="8" /></filter></defs><style>.base { fill: black; font-family: courier; font-size: 8px; }</style><rect width="100%" height="100%" fill="white" ${tokenId < 1000 ? 'stroke="red" stroke-width="10" filter="url(#f1)"' : ''} /><text x="10" y="20" class="base">${hash}</text></svg>`
    return Buffer.from(svgHTML).toString('base64');
}

export default async function handler(req, res) {
    const { tokenId, chain_id } = req.query;

    if (isNaN(Number(tokenId))) {
        res.status(400).send();
        return;
    }

    const hashesContract = getHashesContract(chain_id);
    const hash = await hashesContract.getHash(tokenId);

    if (hash === ethers.constants.HashZero) {
        res.status(404).send();
        return;
    }

    let phrase = "Phrase is being non-fungibilitized... check back soon!";
    const endpoint = process.env.DB_ENDPOINT;
    const key = process.env.DB_KEY;
    if (endpoint && key) {
        const client = new CosmosClient({ endpoint, key });
        const querySpec = {
            query: "SELECT * from t where t.tokenId = @tokenId",
            parameters: [
                {
                    name: "@tokenId",
                    value: tokenId,
                }
            ]
        };

        const dbResult = await client
            .database("hashes")
            .container("scraped_data").items.query(querySpec).fetchAll();

        if (dbResult && dbResult.resources.length) {
            phrase = dbResult.resources[0].phrase;
        }
    }

    res.status(200).json({
        name: `Hashes ID #${tokenId}`,
        description: 'Hashes is a 32-byte hash. Hashes are the core primitives of blockchain technology, thus can serve as the foundation for an infinite number of extensions and applications',
        image: `data:image/svg+xml;base64,${getBase64EncodedSVG(tokenId, hash)}`,
        attributes: HASH_ATTRIBUTES.map(attribute => {
            if (attribute.regex) {
                const match = hex2bin(hash).match(attribute.regex);
                return {
                    trait_type: attribute.descriptionShort,
                    value: match !== null ? match[0].length : 0
                }
            }
            return {
                trait_type: attribute.descriptionShort,
                value: attribute.calculationFunction ? attribute.calculationFunction(hash) : 0
            }
        }).concat([
            {
                trait_type: "Type",
                value: tokenId < 1000 ? "DAO" : "Standard"
            },
            {
                trait_type: "Phrase",
                value: phrase
            }
        ])
    });
}
